import gitlab

# Replace with your GitLab server URL and access token
gitlab_url = 'https://gitlab.com'
access_token = '$GROUP_TOKEN'

gl = gitlab.Gitlab(gitlab_url, private_token=access_token)

# Define the namespace name (e.g., 'your-namespace')
namespace_name = 'dharris'

def list_failed_jobs(project):
    try:
        pipelines = project.pipelines.list(all=True)

        for pipeline in pipelines:
            if pipeline.status == 'failed':
            
                jobs = pipeline.jobs.list()
                for job in jobs:
                    if job.status == 'failed':
                    	print(f"{project.name} - {job.name} - {job.web_url}\n")

    except gitlab.exceptions.GitlabAuthenticationError:
        print("Authentication failed. Please check your access token.")
    except gitlab.exceptions.GitlabListError as e:
        if e.response_code == 403:
            print(f"Access to project or group '{project.name}' in namespace '{namespace_name}' is forbidden.")
        else:
            print(f"GitLab API error: {e}")

def main():
    try:
        namespace = gl.groups.get(namespace_name)
        
        for project in namespace.projects.list(all=True):
            project = gl.projects.get(project.id)
            list_failed_jobs(project)

        for subgroup in namespace.subgroups.list(all=True):
            subgroup = gl.groups.get(subgroup.id)
            for project in subgroup.projects.list(all=True):
                project = gl.projects.get(project.id)
                list_failed_jobs(project)

    except gitlab.exceptions.GitlabAuthenticationError:
        print("Authentication failed. Please check your access token.")
    except gitlab.exceptions.GitlabGetError as e:
        print(f"GitLab API error: {e}")

if __name__ == "__main__":
    print("Project name - Job name - Job URL")
    main()
